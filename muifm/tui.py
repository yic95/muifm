# coding: utf-8
"""The Tui of muifm"""

import curses as c
import os
from time import ctime


# define display status variables
column_x = []  # [x], position of each column
showed_yx_index = [[0, 0], [0, 0]]  # [[start-y, start-x], [len-y, len-x]], index of sorted_dir
cursor_yx = [0, 0]  # [y, x], index of sorted_dir, position of cursor
COL_WIDTHS = [25, 25, 25]
MAXY, MAXX = 0, 0


class FloatingTextInput:
    """A one-line textbox without key recording ability"""
    def __init__(self, screen, y: int, x: int, width: int|float):
        """
        y: start y
        x: start x
        screen: screen to print onto
        width:
            int: fixed width, width
            float: resize enabled, max x id
        """
       
        self.enable_resize = False
        if isinstance(width, float):
            self.enable_resize = True

        # placement vars
        if self.enable_resize:
            self.width = width - x + 1
        else:
            self.width = width

        self.MAXX = self.width + x -1
        self.original_cur_y, self.original_cur_x = y, x

        # render aux vars
        self.cursor_y, self.cursor_x = y, x
        self.x_overshoot = 0
        self.screen = screen

        # user input
        self.reced_keys = ''

    def resize(self, screen_width):
        if not self.enable_resize:
            return
        self.width = width - self.original_cur_x + 1

    def clear(self):
        """
        Set content, status back to default and erase textbox.
        call it before calling self.re_place()
        """
        self.x_overshoot = 0
        self.cursor_y, cursor_x = 0, 0
        self.reced_keys = ''

        self.screen.addstr(self.original_cur_y,
            self.original_cur_x,
            ' '* self.width)  # kill character on self.screen

    def process_key(self, ch: int) -> None:
        """
        Record user input until one of the `stop_rec_key`s is pressed

        Return (Recorded_key, `key`_pressed)
        """

        is_only_reprint = False
        clen = self.cursor_x - self.original_cur_x


        if ch == c.KEY_LEFT:
            if clen > 0:
                self.cursor_x -= 1
            is_only_reprint = True

        if ch == c.KEY_RIGHT:
            if clen < len(self.reced_keys):
                self.cursor_x += 1
            is_only_reprint = True

        if ch == c.KEY_HOME:
            self.cursor_x = self.original_cur_x
            is_only_reprint = True

        if ch == c.KEY_END:
            self.cursor_x = len(self.reced_keys)
            is_only_reprint = True

        if ch == c.KEY_BACKSPACE:
            if clen > 0:  # prevent overshooting
                self.cursor_x -= 1
                self.reced_keys = self.reced_keys[:self.cursor_x:] + self.reced_keys[self.cursor_x + 1::]  # remove cursor where cursor is located
            is_only_reprint = True

        # if ch < 32 or ch > 127 and not is_only_reprint: # block not printable character
        #     continue

        if (not is_only_reprint) and (ch >= 32 and ch <= 127):
            self.reced_keys = self.reced_keys[:clen:] + chr(ch) + self.reced_keys[clen::]

        if not is_only_reprint:
            self.cursor_x += 1

        self.x_overshoot = max(0, self.cursor_x - self.MAXX)  # self.x_overshoot is always positive

    def render(self):
        # used var: cursor_x original_x original_y x_overshoot reced_keys width screen
        self.screen.addstr(self.original_cur_y,
            self.original_cur_x,
            ' '* self.width)  # kill character on self.screen

        self.screen.addstr(self.original_cur_y,
            self.original_cur_x,
            self.reced_keys[self.x_overshoot:min(len(self.reced_keys), self.x_overshoot + self.width):])

        if self.cursor_x < len(self.reced_keys):  # add cursor sign when cursor inside the text
            self.screen.addch(self.original_cur_y, self.cursor_x - self.x_overshoot, self.reced_keys[self.cursor_x], c.A_REVERSE)
        self.screen.refresh()


class Tui:
    def __init__(self, content: list[str, list[list]]):
        self.column_x: list[int] = []  # [x], position of each column
        self.COL_WIDTHS_PERCENT: list[int] = [40, 20, 20]
        self.onscreen_y_index: list[int] = [0, 0]  #  start_y, len_y
        self.cursor_y: int = 0  # y position of cursor
        self.maxy: int = 0
        self.maxx: int = 0  # maximum of x or y id
        self.title_height: int = 1

        self.btm_line_height: int = 1

        self.content: list[str|float] = content[1][1::]
        self.table_title: list[str] = content[1][0]
        self.btm_line: str = content[0]
    def __enter__(self):
        """Setup terminal and create object vars that require stdscr."""
        # setup terminal
        self.stdscr = c.initscr()
        c.curs_set(0)
        
        # correct maxy, maxx
        self.on_resize()

        # define vars that requires self.stdscr
        self.cmd_line: FloatingTextInput = FloatingTextInput(self.stdscr, self.maxy, 0, self.maxx)
        return self
    def __exit__(self, *args):
        """Reset terminal and print out exception if provided."""
        c.curs_set(2)
        c.endwin()
        if args != (None, None, None):
            print(args)
    def on_resize(self):
        """
        Change self.MAXY, self.maxx, self.column_x
        """
        self.maxy, self.maxx = self.stdscr.getmaxyx()
        self.maxy, self.maxx = self.maxy - 1, self.maxx - 1

        # update onscreen... to prevent curses error when len of self.content < self.maxy
        self.onscreen_y_index[1] = min(self.maxy + 1 - self.title_height - self.btm_line_height, len(self.content))
        self.column_x = [0]
        for i in range(len(self.COL_WIDTHS_PERCENT)):
            self.column_x.append(int(self.column_x[i]+(self.COL_WIDTHS_PERCENT[i] / 100 * self.maxx)))
    def get_op(self) -> list[int, str]:
        """
        Get user input and return operation request.
        
        operation:
            0: None
            1: exit
            2: change dir / open file
            3: command
            4: toggle flat mode
        """
        ch: int = self.stdscr.getch()
        if ch == c.KEY_RESIZE:
            self.on_resize()
            self.render_screen()
            return [0, '']
        if ch == 0:
            return [0, '']
        if ch == 106:  # j
            self.process_scroll(True)
            self.render_screen()
            return [0, '']
        if ch == 107:  # k
            self.process_scroll(False)
            self.render_screen()
            return [0, '']
        if ch == 104:  # h
            return [2, '..']
        if ch == 108:  # j
            return [2, self.content[self.cursor_y][0]]
        if ch == 58:  # :
            return [0, '']
        if ch == 10:  # \n
            return [2, self.content[self.cursor_y][0]]
        if ch == 113:
            return [1, '']
        else:
            return [0, '']
    def update_content_and_render(self, content: list[str, list[list]]):
        # update self.content
        self.btm_line = content[0]
        self.table_title = content[1][0]
        self.content = content[1][1::]

        self.on_resize()
        self.cursor_y = 0
        self.onscreen_y_index[0] = 0
        
        self.render_screen()
    def render_screen(self):
        """Render screen"""

        # update self.onscreen_y_index[1] (len) if needed
        # self.onscreen_y_index[1] = min(len(self.content), self.maxy - self.title_height - self.btm_line_height)
        # self.cursor_y = min(self.cursor_y, len(self.content)-1)

        # clear the screen first:
        self.stdscr.erase()

        # render table
        # draw table title
        self.stdscr.addstr(0, 0, ' '*(self.maxx + 1), c.A_REVERSE)
        for xid, x in enumerate(self.table_title):
            self.stdscr.addstr(0, self.column_x[xid], x, c.A_REVERSE)

        # two line spare for bottom line and title
        for ln_id, ln in enumerate(
                self.content[
                    self.onscreen_y_index[0]:
                    sum(self.onscreen_y_index):
                    ]
                ):
            for xid, x in enumerate(ln):
                self.stdscr.addstr(
                        ln_id + self.title_height,
                        self.column_x[xid],
                        str(x)
                        )

        # highlight cursorline
        if len(self.content) != 0:
            for xid, x in enumerate(
                    self.content[self.cursor_y]
                    ):
                self.stdscr.addstr(
                        self.cursor_y - self.onscreen_y_index[0] + self.title_height,
                        self.column_x[xid],
                        str(x),
                        c.A_REVERSE
                        )

        # render bottom line
        self.stdscr.addstr(
                self.maxy,
                0,
                self.btm_line[
                    0:
                    min(self.maxx, len(self.btm_line))
                    ]
                )

        # refresh screen
        self.stdscr.refresh()
    def process_scroll(self, direction: bool):
        """True = down, False = up"""
        if direction:
            if self.cursor_y >= sum(self.onscreen_y_index) - 1 and sum(self.onscreen_y_index) < len(self.content):
                self.onscreen_y_index[0] += 1
                self.cursor_y += 1
            elif self.cursor_y < sum(self.onscreen_y_index) - 1:
                self.cursor_y += 1
        else:
            if self.cursor_y <= self.onscreen_y_index[0] and self.onscreen_y_index[0] > 0:
                self.onscreen_y_index[0] -= 1
                self.cursor_y -= 1
            elif self.cursor_y > 0:
                self.cursor_y -= 1

