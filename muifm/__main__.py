#!/usr/bin/python3
# coding: utf-8

import tui
import fs_util
import os.path
import sys
from os import getcwd, chdir
from os.path import isdir


class MuiFm:
    """Main loop, search, making displying slices"""
    def __init__(self, *args):
        if len(sys.argv) >= 2 and os.path.isdir(sys.argv[1]):
            self.directory = sys.argv[1]
        else:
            self.directory = getcwd()

        self.flat_mode = False
        self.load_content()
    def get_abspath(self, _path):
        return os.path.normpath(os.path.join(self.directory, _path))
    def loop(self):
        with tui.Tui(self.content) as ui:
            ui.update_content_and_render(self.content)
            while True:
                operation = ui.get_op()
                if operation[0] == 1:
                    break
                if operation[0] == 2:
                    if isdir(self.get_abspath(operation[1])):
                        self.directory = self.get_abspath(operation[1])
                        self.load_content()
                    ui.update_content_and_render(self.content)
    def load_content(self) -> None:
        """load content of self.directory into self.content"""
        if isdir(self.directory):
            try:
                if self.flat_mode:
                    self.content = [self.directory, fs_util.flat_entire_dir(self.directory)]
                else:
                    self.content = [self.directory, fs_util.entire_dir(self.directory)]
            except PermissionError:
                self.content = ['Permission denied', []]
        else:
            self.content = ['Directory not exists', []]
    def parse_cmd(self):
        pass

if __name__ == '__main__':
    app = MuiFm()
    app.loop()
