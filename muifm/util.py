# -*- conding: utf-8 -*-
"""tool for rearrange column, search"""


def rearrange_col(content: list[list], order: tuple|list) -> list[list[int], list[list]]:
    """
    Rearrange column of list based on the first row of the list and the list named 'order'.
    Length of sublist of content must be the same.
    """
    # col_lists = [[row[j] for row in content] for j in range(len(content[0]))]
    flat_content = [cell for row in content for cell in row]
    content_width = len(content[0])
    content_height = len(content)

    content_title_id_dict = {d: i for i, d in enumerate(content[0])}
    order_title_id_dict = {d: i for i, d in enumerate(order)}

    find_new_id = [  # find new id using old id
            order_title_id_dict[old_col_title]
            for old_col_title in content[0]
            ]

    id_rev_lookup = [
            content_title_id_dict[new_col_title]
            for new_col_title in order
            ]

    
    #rearranged_col_list = [
    #        col_lists[col_id]
    #        for col_id in id_rev_lookup
    #        ]

    #rearranged_list = [
    #        [col[j] for col in rearranged_col_list]
    #        for j in range(len(rearranged_col_list))
    #        ]
    
    rearranged_flat_list = []
    for col in order:
        rearranged_flat_list.extend(flat_content[content_title_id_dict[col]::content_width])
    
    rearranged_list = [rearranged_flat_list[i::content_height] for i in range(content_height)]

    return [find_new_id, rearranged_list]

