# -*- coding: utf-8 -*-

"""Let tuifm communicate with file system"""

import os
from os.path import sep as SEP
from os.path import dirname, relpath, islink
# from time import ctime
try:
    from pwd import getpwuid  # not exist on MS Windows
except ImportError:
    print('Can\'t import getpwuid')



# TODO config file support
def call_editor(_path: str) -> None:
    """Call the default editor to open `_path`."""
    EDITOR = os.path.expandvars("$EDITOR")
    if EDITOR is None:
        EDITOR = "vi"
    os.system("{} {}".format(EDITOR, _path))


def get_title(column: tuple|list) -> list:
    result = []
    if 'name' in column:
        result.append('name')
    if '_path' in column:
        result.append('_path')
    if 'relpath' in column:
        result.append('relpath')
    if 'mtime' in column:
        result.append('mtime')
    if 'permission' in column:
        result.append('permission')
    if 'owner' in column:
        result.append('owner')
    if 'type' in column:
        result.append('type')
    if 'symlinktarget' in column:
        result.append('symlinktarget')
    return result


def get_file_info(_path: str, column: tuple, _bp: str="") -> list:
    """Return information of a file mentioned in `column`

    Valid value of `column` (and also the order):
        'name': base name
        '_path': _path
        'relpath': relative path based on _bp
        'mtime': last modify time.
                 When _path is a broken symbolic link, it's ''
        'permission': permission (e.g. 700 -> rwx------).
                      When _path is a broken symbolic link, it's ''
        'owner': owner
                 When _path is a broken symbolic link, it's ''
        'type': extend name if `_path` is file,
                '/' if `_path` is a directory,
                '//' if it is a link,
                '///' if it is a broken symbolic link
        'symlinktarget': symbolic link target
                         If `_path` isn't symbolic link, it would be ''
    """
    if not _bp:
        _bp = _path
    item_info_list = []
    is_broken_sym_link = os.path.islink(_path) and not os.path.exists(_path)
    

    if not is_broken_sym_link:
        _item_info = os.stat(_path)
    
    if 'name' in column:
        item_info_list.append(os.path.basename(_path))

    if '_path' in column:
        item_info_list.append(_path)

    if 'relpath' in column:
        item_info_list.append(relpath(_path, _bp))

    if 'mtime' in column:
        if not is_broken_sym_link:
            # item_info_list.append(_item_info.st_mtime)
            item_info_list.append(_item_info.st_mtime)
        else:
            item_info_list.append('')

    if 'permission' in column:
        if not is_broken_sym_link:
            # item_info_list.append(oct(_item_info.st_mode)[-3:])
            item_info_list.append(oct(_item_info.st_mode)[-3:])
        else:
            item_info_list.append('')

    if 'owner' in column:
        if not is_broken_sym_link:
            item_info_list.append(getpwuid(_item_info.st_uid).pw_name)
        else:
            item_info_list.append('')

    if 'type' in column:
        if os.path.islink(_path):
            if os.path.exists(_path):
                item_info_list.append('//')
            else:
                item_info_list.append('///')
        elif os.path.isdir(_path):
            item_info_list.append('/')
        else:
            item_info_list.append(os.path.basename(_path).rsplit('.')[-1])

    if 'symlinktarget' in column:
        if os.path.islink(_path):
            item_info_list.append(os.readlink(_path))
        else:
            item_info_list.append('')

    return item_info_list


def entire_dir(
        _path: str,
        column: tuple = ('name', 'mtime', 'permission', 'owner')
        ) -> list:
    """A fancy version of os.listdir.

    Return every `column`(metadata, permission, etc.)
    of every file or directory under the directory `_path`
    by calling `get_file_info`.

    column item placement doesn't matter.
    _path: absolute path. Use '~' to represent $HOME.
    """
    _path = os.path.expanduser(_path)
    # _dir = os.path.dirname(_path)
    # contents = [f'{_path}{SEP}{i}' for i in os.listdir(_path)]
    dir_item_table = [
            get_file_info(_item, column)
            for _item in [
                f'{_path}{SEP}{i}' 
                for i in os.listdir(_path)
                ]
            ]
    dir_item_table.insert(0, get_title(column))

    return dir_item_table



def flat_entire_dir(
        _path: str,
        column: tuple = ('relpath', 'mtime', 'permission', 'owner'),
        exclude_dir: set = {'.git'}
        ) -> list:
    """A fancy version of os.listdir.

    Return every `column`(metadata, permission, etc.)
    of every file under the directory `_path`
    by calling `get_file_info`.

    Column item placement doesn't matter.
    _path: absolute path. Use '~' to represent $HOME.
    """
    _path = os.path.expanduser(_path)

    # flatent directory
    contents = [f'{_path}{SEP}{i}' for i in os.listdir(_path) if i not in exclude_dir]
    contents_tmp = []

    # TODO Improve performance
    while True:
        for i in contents:
            if os.path.isdir(i) and i not in exclude_dir and not islink(i):
                contents_tmp.extend([f'{i}{SEP}{j}' for j in os.listdir(i)])
            else:
                contents_tmp.append(i)
        if contents == contents_tmp:
            break
        else:
            contents, contents_tmp = contents_tmp, contents
            contents_tmp.clear()

    dir_item_table = [get_file_info(_item, column, _path) for _item in contents]
    dir_item_table.insert(0, get_title(column))

    return dir_item_table


def create_file(_path: str) -> [bool, str]:
    """Create file `_path`. Return [False, error_message] if fail"""
    _path = os.path.expanduser(_path)
    try:
        with open(_path, 'x'):
            pass
    except FileExistsError:
        return [False, 'file exists']
    return [True, '']
